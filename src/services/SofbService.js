  import Vue from 'vue'

  export default {
    getMagazines (succes, error) {
      Vue.http.get('https://api.stokedonfixedbikes.com/api/magazines').then(
        (response) => {
          succes(response.data)
        },
        (response) => {
          error(response)
        }
      )
    },
    getMagazine (id) {
      return Vue.http.get('https://api.stokedonfixedbikes.com/api/magazines/' + id)
    },
    getLinks (succes, error) {
      Vue.http.get('https://api.stokedonfixedbikes.com/api/links').then(
        (response) => {
          succes(response.data)
        },
        (response) => {
          error(response)
        }
      )
    },
    getVideos (succes, error) {
      Vue.http.get('https://api.stokedonfixedbikes.com/api/videos').then(
        (response) => {
          succes(response.data)
        },
        (response) => {
          error(response)
        }
      )
    }

    // getMagazines(){ return this.$http.get('/someUrl')}
  }
