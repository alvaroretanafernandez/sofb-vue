import SofbService from '../../services/SofbService'
import SubHeader from '../sub-header/sub-header.vue'
import SelectedVideo from './video/video-item.vue'
export default {
  components: {SubHeader, SelectedVideo},

  data () {
    return {
      loaded: false,
      showIframe: false,
      title: 'Videos',
      videos: [],
      selected: '',
      YT_URL: 'https://www.youtube.com/embed/',
      VI_URL: 'https://player.vimeo.com/video/'
    }
  },
  mounted: function () {
    this.getVideos()
  },
  watch: {
    selected: function (newVal) {
      this.selected = newVal
    }
  },
  methods: {
    getVideos: function () {
      var me = this
      SofbService.getVideos((data) => {
        me.loaded = true
        me.videos = data
      }, (response) => {
        // toastr.error("Oops, " + response.status + " " + response.statusText, "Users", toastrOptions);
      })
    },
    loadVideo: function (video) {
      if (video.type === 'youtube') {
        this.selected = this.YT_URL + video.videoId
      } else {
        this.selected = this.VI_URL + video.videoId + '?autoplay=0'
      }
      this.showIframe = true
    },
    closeVideo: function () {
      this.showIframe = false
    }
  }

}
