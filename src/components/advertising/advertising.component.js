/**
 * Created by arfs on 21/05/2017.
 */
import SubHeader from '../sub-header/sub-header.vue'
export default {
  components: {SubHeader},
  data () {
    return {
      title: 'Gallery',
      oldAdverts: [
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bagaboo/1.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bagaboo/2.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/breakbrake17/1.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/breakbrake17/2.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/breakbrake17/3.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/fabike/1.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/fabike/2.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/fabike/3.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/factory_five/1.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/factory_five/2.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/factory_five/3.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/fixdich/1.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/fyxation/fyxation.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/godandfamous/1.gif'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/londonbicycleworkshop/1.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/londonbicycleworkshop/2.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/londonbicycleworkshop/3.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/londonbicycleworkshop/4.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/londonbicycleworkshop/5.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/londonbicycleworkshop/6.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/poloandbike/1.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/poloandbike/2.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/poloandbike/3.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/poloandbike/4.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/purefix/1.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/purefix/2.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/purefix/3.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/sanmarco/1.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/sanmarco/2.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/sanmarco/3.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/sanmarco/4.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/sanmarco/5.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/sanmarco/6.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/1.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/2.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/3.jpg'
        },
        {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/4.jpg'
        }
      ],
      adverts: [
        {
          'icon': 'fa-thumbs-up',
          'title': 'Support',
          'desc': 'Support our magazine by advertising with us. SOFB online mag is free, you can support the team ' +
          '<a class="text-gray-dark" target=\'_blank\' href=\'https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=EGAFDK6FTKQ7S\'><strong>here</strong></a>.'
        },
        {
          'icon': 'fa-newspaper-o',
          'title': 'Magazine',
          'desc': 'Place your products, products reviews, adverts, etc in our magazine, different sizes and formats available.'
        },
        {
          'icon': 'fa-laptop',
          'title': 'Website',
          'desc': 'Web Banners. Static image or slider*. Minimum 2 months.*Slider (max. of 6 different images/links).'
        },
        {
          'icon': 'fa-gift',
          'title': 'Offers',
          'desc': 'We offer different advertising options. You can choose ads in the magazine or/and website.'
        }
      ],

      banner: 'https://s3.eu-central-1.amazonaws.com/sofbheaders/12.jpg'
    }
  },
  methods: {}
}
