/**
 * Created by arfs on 21/05/2017.
 */
import SubHeader from '../sub-header/sub-header.vue'
export default {
  components: {SubHeader},
  data () {
    return {
      title: 'Gallery',
      gallerySlides: [
        {
          'id': '1',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/redi1.jpg'
        },
        {
          'id': '2',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/mtb1.jpg'
        },
        {
          'id': '3',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/prague1.jpg'
        },
        {
          'id': '4',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/respoke1.jpg'
        },
        {
          'id': '5',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/red1.jpg'
        },
        {
          'id': '6',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/mini1.jpg'
        },
        {
          'id': '7',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/mini2.jpg'
        },
        {
          'id': '8',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/mini3.jpg'
        },
        {
          'id': '9',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/lisbon1.jpg'
        },
        {
          'id': '10',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/krakow1.jpg'
        }
      ],

      banner: 'https://s3.eu-central-1.amazonaws.com/sofbheaders/12.jpg'
    }
  },
  methods: {}
}
