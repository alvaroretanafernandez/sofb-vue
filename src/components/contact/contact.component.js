/**
 * Created by arfs on 21/05/2017.
 */
import SubHeader from '../sub-header/sub-header.vue'
export default {
  components: {SubHeader},
  data () {
    return {
      title: 'Contact',
      sofbAbout: {
        'h2': 'Founder & Editor',
        'h3': 'Greg Falski',
        'p': 'Greg has been an avid rider of all types of bikes since he was a kid. ' +
        'He studied photography in London and combined his passion for bikes and photography ' +
        'in his magazine Stoked On Fixed Bikes.He is an established sports photographer, and has shot ' +
        'with brands like Vans, Gumball 3000,Ted James Design, Fixed Gear London, Tokyo Fixed, NS Bikes, Charge Bikes, BB 17,Don´t panic magazine, Karpiel Bikes...just to mention a few.'
      },
      sofbTeam: {
        'h2': 'The Team',
        'p': 'Stoked On Fixed Bikes',
        'members': [{
          'name': 'Greg Falski',
          'avatar': 'https://s3-us-west-2.amazonaws.com/sofb-landing/g.jpg',
          'role': 'Founder',
          'desc': 'Rider, photographer and editor.'
        }, {
          'name': 'Anto But',
          'avatar': 'https://theradavist.com/old/2010/04/13/AntoButLayout-PINP-thumb.jpg',
          'role': 'Collaborator',
          'desc': 'Legendary London fgfs and bmx rider.'
        }, {
          'name': 'Alvaro Retana',
          'avatar': 'https://lh3.googleusercontent.com/-TUMn6BMvN3A/Ula2QZaQP6I/AAAAAAAAARs/hd3iWx_UD1ctTkjhXRuEeUPc0goypAcCQCEw/w140-h140-p/image.jpeg',
          'role': 'Website',
          'desc': 'Javascript full-stack developer.'
        }]
      },
      banner: 'https://s3.eu-central-1.amazonaws.com/sofbheaders/8.jpg'
    }
  },
  methods: {}
}
