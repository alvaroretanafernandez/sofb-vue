export default {
  props: ['item'],
  data () {
    return {
      loaded: false
    }
  },
  mounted: function () {
    this.loaded = true
  }
}
