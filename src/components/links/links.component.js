import SofbService from '../../services/SofbService'
import SubHeader from '../sub-header/sub-header.vue'
import LinkItem from './link-item/link-item.vue'
export default {
  components: {SubHeader, LinkItem},
  data () {
    return {
      loaded: false,
      title: 'Links',
      links: []
    }
  },
  mounted: function () {
    this.getLinks()
  },
  methods: {
    getLinks: function () {
      var me = this
      SofbService.getLinks((data) => {
        me.loaded = true
        me.links = data
      }, (response) => {
        // toastr.error("Oops, " + response.status + " " + response.statusText, "Users", toastrOptions);
      })
    }
  }

}
