import SofbService from '../../services/SofbService'
import SubHeader from '../sub-header/sub-header.vue'
import MagItem from './magazine-item/magazine-item.vue'
export default {
  components: {SubHeader, MagItem},
  data () {
    return {
      loaded: false,
      title: 'Magazines',
      magazines: []
    }
  },
  mounted: function () {
    this.getMagazines()
  },
  methods: {
    getMagazines: function () {
      var me = this
      SofbService.getMagazines((data) => {
        me.loaded = true
        me.magazines = data
      }, (response) => {
        // toastr.error("Oops, " + response.status + " " + response.statusText, "Users", toastrOptions);
      })
    }
  }

}
