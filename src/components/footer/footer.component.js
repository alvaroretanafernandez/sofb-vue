export default {
  data () {
    return {
      arf: 'freepowder',
      footerMenu: [
        {
          'title': 'Home',
          'state': '/'
        },
        {
          'title': 'Magazines',
          'state': '/magazines'
        },

        {
          'title': 'Gallery',
          'state': '/gallery'
        },
        {
          'title': 'Videos',
          'state': '/videos'
        },
        {
          'title': 'Advertising',
          'state': '/advertising'
        },
        {
          'title': 'Links',
          'state': '/links'
        },
        {
          'title': 'About',
          'state': '/about'
        },
        {
          'title': 'Contact',
          'state': '/contact'
        }
      ]
    }
  }
}
