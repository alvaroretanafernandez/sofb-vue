/**
 * Created by arfs on 21/05/2017.
 */
export default {
  data () {
    return {
      title: 'About',
      sofbAdPlaceSlides: [
        {
          'id': '1',
          'type': 'Place your add here.<br><a href="contact">Contact Us</a> for more information.',
          'url': 'https://www.stokedonfixedbikes.com/',
          'img': 'https://placehold.it/280x250'
        },
        {
          'id': '2',
          'type': 'Promote your products with <br> stoked on fixed bikes.',
          'url': 'https://www.stokedonfixedbikes.com/',
          'img': 'https://placehold.it/280x250'
        },
        {
          'id': '2',
          'type': 'Web & Magazine Offers. <br> <a href="contact">Contact Us</a> for more information.',
          'url': 'https://www.stokedonfixedbikes.com/',
          'img': 'https://placehold.it/280x250?text=SOFB'
        }
      ],

      sofbAdSlides: [
        {
          'id': '1',
          'type': 'sofb',
          'url': 'https://www.stokedonfixedbikes.com/',
          'img': '/assets/img/0.png'
        },
        {
          'id': '2',
          'type': 'sofb',
          'url': 'https://www.stokedonfixedbikes.com/',
          'img': '/assets/img/0w.png'
        }
      ],

      poloAndBikeSlides: [
        {
          'id': '1',
          'type': 'poloandbike',
          'url': 'https://www.poloandbike.com/',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/poloandbike/1.jpg'
        },

        {
          'id': '2',
          'type': 'poloandbike',
          'url': 'https://www.poloandbike.com/',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/poloandbike/2.jpg'
        },
        {
          'id': '3',
          'type': 'poloandbike',
          'url': 'https://www.poloandbike.com/',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/poloandbike/3.jpg'
        },
        {
          'id': '4',
          'type': 'poloandbike',
          'url': 'https://www.poloandbike.com/',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/poloandbike/4.jpg'
        }
      ],

      kappsteinSlides: [
        {
          'id': '1',
          'type': 'kappstein',
          'url': 'https://www.kappstein.de/',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/kappstein/1.jpg'
        },

        {
          'id': '2',
          'type': 'kappstein',
          'url': 'https://www.kappstein.de/',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/kappstein/2.jpg'
        },
        {
          'id': '3',
          'type': 'kappstein',
          'url': 'https://www.kappstein.de/',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/kappstein/3.jpg'
        }
      ],

      bombtrackSlides: [
        {
          'id': '1',
          'type': 'bombtrack',
          'url': 'https://www.bagaboo.com/',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/1.jpg'
        },

        {
          'id': '2',
          'type': 'bombtrack',
          'url': 'https://www.bombtrack.com/',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/2.jpg'
        },
        {
          'id': '3',
          'type': 'bombtrack',
          'url': 'https://www.bombtrack.com/',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/3.jpg'
        },
        {
          'id': '4',
          'type': 'bombtrack',
          'url': 'https://www.bombtrack.com/',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/4.jpg'
        },
        {
          'id': '5',
          'type': 'bombtrack',
          'url': 'https://www.bombtrack.com/',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/5.jpg'
        }
      ],

      banner: 'https://s3.eu-central-1.amazonaws.com/sofbheaders/12.jpg'
    }
  },
  methods: {}
}
