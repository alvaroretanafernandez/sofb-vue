import SofbService from '../../services/SofbService'
import magazineSofb from './magazine-sofb/magazine-sofb.vue'
import PagerSofb from './pager-sofb/pager-sofb.vue'

export default {
  components: {magazineSofb, PagerSofb},
  data () {
    return {
      loaded: false,
      prev: Number(this.$route.params.id) - 1,
      next: Number(this.$route.params.id) + 1,
      id: this.$route.params.id,
      frame: '',
      published: '',
      title: 'Magazine',
      mag: null
    }
  },

  beforeRouteEnter (to, from, next) {
    SofbService.getMagazine(to.params.id)
      .then((data) => {
        next(vm => vm.setData(data.body))
      }, error => {
        console.log(error)
      })
  },
  // when route changes and this component is already rendered,
  // the logic will be slightly different.
  beforeRouteUpdate (to, from, next) {
    this.mag = null
    this.frame = null
    this.published = null
    SofbService.getMagazine(to.params.id)
      .then(data => {
        this.setData(data.body)
        next()
      }, error => {
        console.log(error)
      })
  },
  methods: {
    setData (data) {
      this.mag = data
      this.frame = data.frame
      this.published = data.published
      this.loaded = true
      this.id = data.id
      this.prev = Number(data.id) - 1
      this.next = Number(data.id) + 1
    }
  }
}
