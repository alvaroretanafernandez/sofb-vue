export default {
  props: ['frame', 'published'],
  data () {
    return {
      loaded: false
    }
  },
  mounted: function () {
    this.loaded = true
  },
  updated: function () {
    this.loaded = true
  }
}
