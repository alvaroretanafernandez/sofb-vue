import Vue from 'vue'
import Router from 'vue-router'
import Hello from 'components/Hello'
import Magazines from 'components/magazines/magazines.page'
import Magazine from 'components/magazine/magazine.page'
import Links from 'components/links/links.page'
import Videos from 'components/videos/videos'
import About from 'components/about/about.page'
import Contact from 'components/contact/contact.page'
import Gallery from 'components/gallery/gallery'
import Advertising from 'components/advertising/advertising'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/magazines',
      name: 'Magazines',
      component: Magazines
    },
    {
      path: '/magazine/:id',
      name: 'Magazine',
      component: Magazine
    },
    {
      path: '/links',
      name: 'Links',
      component: Links
    },
    {
      path: '/videos',
      name: 'Videos',
      component: Videos
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/contact',
      name: 'Contact',
      component: Contact
    },
    {
      path: '/gallery',
      name: 'Gallery',
      component: Gallery
    },
    {
      path: '/advertising',
      name: 'Advertising',
      component: Advertising
    }
  ]
})
